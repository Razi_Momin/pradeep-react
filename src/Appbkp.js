import React, { Component } from 'react';
import { webService } from './webService';
import Box from './Box';
import NavBar from './NavBar';
import Home from './Home';
import About from './About';
import LoginCheck from './LoginCheck';
import Profile from './Profile';
import SignUp from './SignUp';
import Step2 from './Step2';
import './App.css';
import { CommonContext } from './components/CommonContext';
import Main from './components/Main';
import Update from './components/Update';
import querystring from 'querystring';
import { BrowserRouter, Route } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';

export default class App extends Component {
  constructor(props) {
    // alert(CryptoJS.MD5("Message"))
    super();
    this.updateColor = (color) => {
      this.setState({
        color: color
      })
    }
    this.state = {
      mobile_no: '',
      color: 'green',
      updateColor: this.updateColor
    }

    // console.log(gVars.API_BASE_URL);
  }
  handleChange = (e) => {
    // console.log(1);
    this.setState({
      [e.target.id]: e.target.value
    });
  }
  handleSubmit = (e) => {
    e.preventDefault();
    //console.warn(this.state);

    let apiEndpoint = 'Oauth/primaryStep';
    let header = webService.getHeaders({ 'ID': 1 })
    // webService.get(apiEndpoint,header).then((response) => {
    //   if (response.status === 200) {

    //   } else {

    //   }
    //  // console.log(response)
    // })
    let postArray = { mobile_no: this.state.mobile_no };
    // console.log(postArray)
    webService.post(apiEndpoint, querystring.stringify(postArray), header).then((response) => {
      if (response.status === 200) {
        //   alert('success');
      } else {

      }
      //console.log(response.data)
    })
  }

  render() {
    return (

      <div className="App">
        <CommonContext.Provider value={this.state}>
          <p>content api</p>
          <Header />
          <Main />
          <Update />
          <Footer />
        </CommonContext.Provider>
      
        <BrowserRouter>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/logincheck" component={LoginCheck} />
          <Route path="/signup" component={SignUp} />
          <Route path="/step2" component={Step2} />
          <Route path="/profile/:matchid/:seriesid/:team1id/:team2id" component={Profile} />
          {/* <NavBar /> */}
        </BrowserRouter>
        {/* <form onSubmit={this.handleSubmit}>
          <input onChange={this.handleChange} id="mobile_no" placeholder="mobile no" type="tel" />
          <br></br>
          <input type="submit" value="submit" />
        </form> */}
        {/* <Box content="<div>Some content</div>" /> */}
      </div>
    );
  }
}








