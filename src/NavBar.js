import React, { Component } from 'react'
import { Link, NavLink, withRouter } from 'react-router-dom';
function NavBar(props) {
    // console.log(props);
    // setTimeout(function () {
    //     props.history.push('/about');
    // }, 2000)
    return (
        <nav>
            <ul>
                <li>
                    <NavLink to="/">
                        Home
                    </NavLink>

                </li>
                <li>
                    <NavLink to="/about">
                        About US
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/profile">
                        Profile
                    </NavLink>
                </li>
            </ul>
        </nav>
    )
}
export default withRouter(NavBar);
