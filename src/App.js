import React from 'react'
import Main from './reduxcomponent/Main'
import {BrowserRouter, Route} from 'react-router-dom';
import About from './reduxcomponent/About';
import Home from './reduxcomponent/Home';

export default function App() {
    return (
        <div>
            <BrowserRouter>
                <Route exact path="/">
                    <Main/>
                </Route>
                {/* <Route path="/about" component={About} />
                <Route path="/home" component={Home} /> */}
            </BrowserRouter>
        </div>
    )
}
