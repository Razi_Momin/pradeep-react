import {combineReducers} from "redux";
import {HomeReducer} from './home.reducer';

const reducer = combineReducers({HomeReducer});

export default reducer;