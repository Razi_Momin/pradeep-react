import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
class LoginCheck extends Component {
    constructor(props) {
        super();
        this.state = {
            mobile_no: ''
        }
        // console.log(gVars.API_BASE_URL);
    }
    loginCheck = (e) => {
        let mobile_no = this.state.mobile_no
        this.props.history.push({
            pathname: '/signup',
            // mobile_no: mobile_no,
            // search: '?query=abc',
            state: { mobile_no: mobile_no }
        });
    }
    updateValue = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    render() {
        return (
            <div>
                <input type="text" id="mobile_no" onChange={this.updateValue} placeholder="Enter Mobile Number"></input>
                <button onClick={this.loginCheck}>Submit</button>
            </div>
        )
    }
}

export default withRouter(LoginCheck);