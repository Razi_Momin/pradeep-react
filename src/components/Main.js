import React, { Component } from 'react'

import { CommonContext } from './CommonContext';
export default class Main extends Component {
    constructor(props) {
        // alert(CryptoJS.MD5("Message"))
        super();
        this.state = {
            mobile_no: '',
            color: null,
            updateColor: this.updateColor
        }
        this.updateColor = () => {
            this.setState({
                color: 'red'
            })
        }
        // console.log(gVars.API_BASE_URL);
    }
    render() {
        return (
            <div>
                <CommonContext.Consumer>
                    {
                        ({ color }) => (
                            <h1 style={{ backgroundColor: color }}>Hello this is main page</h1>
                        )
                    }
                </CommonContext.Consumer>
            </div>
        )
    }
}
