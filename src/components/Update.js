import React from 'react'
import { CommonContext } from './CommonContext';
function Update() {
    return (
        <div>
            <CommonContext.Consumer>
                {
                    ({ updateColor }) => (
                        <button onClick={() => updateColor('yellow')}>Update</button>
                    )
                }
            </CommonContext.Consumer>
        </div>
    )
}
export default Update
