
import axios from 'axios';

const baseApiUrl = 'http://localhost/api/';

export const webService = {
	get,
	post,
	put,
	deleteDetail,
	getHeaders
};

function get(apiEndpoint,headers) { 
	return axios
		.get(baseApiUrl + apiEndpoint, {headers})
		.then((response) => {
			return response;
		})
		.catch((err) => {
			return ErrorRespond(err);
		});
}

function post(apiEndpoint, payload, headers) {

	return axios
		.post(baseApiUrl + apiEndpoint, payload, {headers})
		.then((response) => {
			return response;
		})
		.catch((err) => {
			return ErrorRespond(err);
		});
}

function put(apiEndpoint, payload, headers) {
	return axios
		.put(baseApiUrl + apiEndpoint, payload, {headers})
		.then((response) => {
			return response;
		})
		.catch((err) => {
			return ErrorRespond(err);
		});
}

function deleteDetail(apiEndpoint, headers) {
	return axios
		.delete(baseApiUrl + apiEndpoint, {headers})
		.then((response) => {
			return response;
		})
		.catch((err) => {
			return ErrorRespond(err);
		});
}

function ErrorRespond(error) {
	return { data: { type: 'ERROR', message: error, status: 'NET_ERROR' } };
}


function getHeaders(otherHeader = []) {
	let options = {
		Accept: 'application/json',
		// "Content-Type": "application/x-www-form-urlencoded",
		'Client-Service': 'mobile-client-qc',
		'Auth-key': 'playerzpotrestapi',
	};
	options = { ...otherHeader, ...options }; 
	return options;
}