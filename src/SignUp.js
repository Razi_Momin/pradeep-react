import React, { Component } from 'react'
import Step2 from './Step2';

export default class SignUp extends Component {
    constructor() {
        super();
        this.state = {
            'mobile_no': '',
            'email': '',
        }
    }
    componentDidMount(){
        this.state = {
            'mobile_no': '',
            'email': '',
        }
    }
    Step2Call = () => {
        let mobile_no = this.state.mobile_no;
        let email = this.state.email;
        // console.log(this.state);
        // return false;
        this.props.history.push({
            pathname: '/Step2',
            // mobile_no: mobile_no,
            // search: '?query=abc',
            state: { mobile_no: mobile_no, email: email }
        });
    }
    updateValue = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    render() {
        console.log(this.props);
        // let mobile_no = this.props.location.state.mobile_no;
        // console.log(this.props.location.state.mobile_no);
        let mobile_no = '';
        if (this.props.location.state !== undefined) {
            mobile_no = this.props.location.state.mobile_no;
        }
        return (
            <div>
                <input autoComplete="off" type="text" id="mobile_no" onChange={this.updateValue} onBlur={this.updateValue} defaultValue={mobile_no} placeholder="Mobile No" id="mobile_no"></input>
                <br></br>
                <input type="email" onChange={this.updateValue} placeholder="email" id="email"></input>
                <br></br>
                <button onClick={this.Step2Call}>Next Step</button>
            </div>
        )
    }
}
