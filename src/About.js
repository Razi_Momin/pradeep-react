import React from 'react';
const About = (props) => {
    // console.log(props);

    return <div>
        <h1>About Page</h1>
        <button onClick={() => { props.history.push('/profile/1/2/3/4') }}>Go to Profile</button>
        <button onClick={() => {
            props.history.push({
                pathname: '/profile/1/2/3/4',
                mobileno: '9823771077',
            });
        }}>Go to Profile with props</button>
    </div>
}
export default About;