import React from 'react'

export default function Box(props) {

    const mystyle = {
        height: "200px",
        width: "200px",
        padding: "10px",
        border: "1px solid #333",
        margin: "20px auto"
    };
    return (
    <div style={mystyle} dangerouslySetInnerHTML={{ __html: props.content }} />
    )
}



