// import {createStore} from "redux";
// import reducer from './reducer';
//
// const store = createStore(reducer);
//
// export default store;


import {applyMiddleware, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import rootReducer from './reducer';

const loggerMiddleware = createLogger();

const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
}) : compose;

const enhancerDev = composeEnhancers(applyMiddleware(thunkMiddleware, loggerMiddleware));
const enhancerProd = composeEnhancers(applyMiddleware(thunkMiddleware));
const devStore = createStore(rootReducer, process.env.NODE_ENV === 'production' ? enhancerProd : enhancerDev);

export const store = devStore;