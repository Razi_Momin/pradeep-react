const initialState = {};

export function HomeReducer(state = initialState, action) {
    switch (action.type) {
        case 'HOME':
            return {...state, text: action.text};
        case 'ABOUT':
            return {...state, text: action.text};
        default:
            return {};
    }
}