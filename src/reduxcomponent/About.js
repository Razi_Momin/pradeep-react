import React from 'react'
import {store} from "../store";

export default function About() {
    const {dispatch} = store;

    const clickBtn = () => {
        dispatch({type: 'ABOUT', text: 'This is from About click btn'});
    };

    return (
        <div>
            <button onClick={clickBtn}>Change Content from about</button>
        </div>
    )
}
