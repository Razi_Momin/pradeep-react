import React from 'react'
import {store} from './../store';
export default function Home() {
    const {dispatch} = store;
    const clickBtn = () => {
        dispatch({type: 'HOME', text: 'This is from Home click btn'});
    };
    return (
        <div>
            <button onClick={clickBtn}>Change Content from Home</button>
        </div>
    )
}
