import React from 'react'
import {connect} from "react-redux";
import Home from './Home'
import About from './About'

function mapStateToProps(state) {
    const {HomeReducer} = state;
    return {HomeReducer}
}
function Main(props) {

    console.log('===============',props)
    let text = props.HomeReducer.text
    return (
        <div>
            {text}
            <br/>
            <br/>
            <Home/>
            <About/>
            {/* <button onClick={() => { props.history.push('/about') }}>Go to about page</button>
            <button onClick={() => { props.history.push('/home') }}>Go to home page</button> */}
        </div>
    )
}
export default connect(mapStateToProps)(Main)
