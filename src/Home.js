import React from 'react';
const Home = (props) => {
    // setTimeout(function () {
    //   props.history.push('/about');
    // }, 2000)
    return <div>
        <h1>Home Page</h1>
        <button onClick={() => { props.history.push('/about') }}>Go to ABout</button>
    </div>
}
export default Home;